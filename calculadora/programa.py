from ui import menu, pedir_datos, mostrar_resultado, SUMAR, RESTAR, MULTIPLICAR, DIVIDIR, SALIR, mostrar_opcion_incorrecta
from calculadora import sumar, restar, multiplicar, dividir

opcion = 0
while opcion != SALIR:
    # Muestra el menu
    opcion = menu()

    if opcion == SUMAR:
        # Pide los datos al usuario
        num1, num2 = pedir_datos()
        # Ejecuta la operacion
        res = sumar(num1, num2)
        # Muestra el resultado
        mostrar_resultado(SUMAR, num1, num2, res)
    elif opcion == RESTAR:
        # Pide los datos al usuario
        num1, num2 = pedir_datos()
        # Ejecuta la operacion
        res = restar(num1, num2)
        # Muestra el resultado
        mostrar_resultado(RESTAR, num1, num2, res)
    elif opcion == MULTIPLICAR:
        # Pide los datos al usuario
        num1, num2 = pedir_datos()
        # Ejecuta la operacion
        res = multiplicar(num1, num2)
        # Muestra el resultado
        mostrar_resultado(MULTIPLICAR, num1, num2, res)
    elif opcion == DIVIDIR:
        # Pide los datos al usuario
        num1, num2 = pedir_datos()
        # Ejecuta la operacion
        res = dividir(num1, num2)
        # Muestra el resultado
        mostrar_resultado(DIVIDIR, num1, num2, res)
    elif opcion == SALIR:
        # salir
        mostrar_opcion_incorrecta()
    else:
        print('Opcion erronea!')
