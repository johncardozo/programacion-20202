SUMAR = 1
RESTAR = 2
MULTIPLICAR = 3
DIVIDIR = 4
SALIR = 5

def menu():
    '''
    Muestra el menu principal de la aplicacion
    '''
    # Muestra el menu
    print('\nCALCULADORA')
    print('1. Sumar')
    print('2. Restar')
    print('3. Multiplicar')
    print('4. Dividir')
    print('5. Salir')

    # Pide la opcion al usuario
    # try:
    #     opcion = int(input('Digite su opcion... '))
    #     return opcion
    # except:
    #     return -1
    
    # Pide la opcion al usuario
    opcion_digitada = input('Digite su opcion... ')
    # Verifica que todos los carcateres digitados sean numericos
    if opcion_digitada.isnumeric():
        return int(opcion_digitada)
    else:
        return -1


def pedir_datos():
    '''
    Pide datos al usuario
    '''
    # Pide los datos al usuario
    try:
        a = float(input('Digite el numero 1: '))
        b = float(input('Digite el numero 2: '))
        # Retornar los datos
        return a, b
    except:
        return false, false


def mostrar_resultado(operacion, num1, num2, res):
    mensaje_operacion = ''
    if operacion == SUMAR:
        mensaje_operacion = 'suma'
    elif operacion == RESTAR:
        mensaje_operacion = 'resta'
    elif operacion == MULTIPLICAR:
        mensaje_operacion = 'multiplicacion'
    elif operacion == DIVIDIR:
        mensaje_operacion = 'division'

    # Formatea el mensaje
    mensaje = f'La {mensaje_operacion} entre {num1} y {num2} es {res}'
    # Muestra el mensaje de resultado
    print(mensaje)

def mostrar_opcion_incorrecta():
    print('Muchas gracias por usar la calculadora')



