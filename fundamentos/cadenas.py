cadena1 = 'universidad santo tomas'
cadena2 = "usta"
print(cadena1)
print(cadena2)

# Caracter de escape
# \' \" \n \t \\
frase = 'I\tdon\'t know\nhow to program'
print(frase)

# RAW string
ruta = r'c:\datos\nomina.xls'
print(ruta)

# Acceso a una posición de la cadena por el índice
# 0=u, 1=s, 2=t, 3=a
print(cadena2[0])
print(cadena2[1])
print(cadena2[3])
# Obtiene el ultimo caracter
print(cadena2[-1])
print(cadena2[-2])
print('=' * 20)
# Genera error
#cadena2[1] = 't'
# el parametro de la derecha no es incluído
print(cadena1[0:5])
# Obtener una subcadena
print(cadena1[4:10])
print(cadena1[:7])
print(cadena1[5:])
print(cadena1[-3:])
