# Hacer un programa en Python que pida al usuario:
# la base del triángulo
# la altura de un triángulo 
# y muestre por pantalla el área del triángulo
print('Calculadora de Área del Triángulo')

# Pide datos al usuario
base = input('Digite la base: ')
altura = input('Digite la altura: ')

# Convierte los datos del usuario a float
b = float(base)
h = float(altura)

# Calcula el área del triángulo
area = (b * h) / 2

# Muestra el área del triángulo en la consola
print(area)

