edad = 5

es_adulto = not edad == 18
#print(es_adulto)

esta_en_rango = edad >= 10 or edad <= 20
#print(esta_en_rango)

# if -> una opción
# if-else -> 2 opciones
# if-else-if -> n opciones

edad = 35
if edad == 18:
    print('Tiene 18 años!')

if edad > 18:
    print('Es adulto')
else:
    print('No es adulto')


if edad == 10:
    print('tiene 10 años')
elif edad == 15:
    print('tiene 15 años')
elif edad == 17:
    print('tiene 17 años')
elif edad == 5:
    print('tiene 5 años')
elif edad == 35:
    print('tiene 35 años')
else:
    print('NO FUE NINGUNA DE LAS OPCIONES')
