cadena = 'Python es un lenguaje'
print(cadena)

# Obtiene la longitud de una cadena
longitud = len(cadena)
print(longitud)

# Analisis de cadenas
termina = cadena.endswith('aje')
print(termina)
inicia = cadena.startswith('Py')
print(inicia)
existe = 'es' in cadena
print(existe)
posicion = cadena.find('es')
print(posicion)

# Formato de cadena
nombre = 'Catalina'
edad = 18
mensaje1 = 'La estudiante ' + nombre + ' tiene ' + str(edad) + ' años'
mensaje2 = f'La estudiante {nombre} tiene {edad} años.'
print(mensaje1)
print(mensaje2)

print(cadena.title())

# Cuenta la cantidad de ocurrencias
busqueda = 'n'
cantidad = cadena.count(busqueda)
print(f'"{busqueda}" está {cantidad} veces en "{cadena}"')

print(cadena.replace('e', '3'))
print(cadena.lower())
print(cadena.upper())

# Análisi de Strings
password = 'ABCabc'
print(f'{password} es alfanumerico? = {password.isalnum()}')
print(f'{password} es alfabético? = {password.isalpha()}')
print(f'{password} es numérico? = {password.isdecimal()}')
print(f'{password} tiene solo minúsculas? = {password.islower()}')
print(f'{password} tiene solo MAYÚSCULAS? = {password.isupper()}')



