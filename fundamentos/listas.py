# Declaracion de una lista
lista1 = []
lista2 = [10, 20, 30]
lista3 = [100, 'maria', True, 3.1416]

print(lista1)
print(lista2)
print(lista3)

# Longitud de una lista
lista = [100, 200, 300, 400, 500]
print(lista)
print(len(lista))
print(lista[2])
# Concatenar listas
resultado = lista + [5, 6, 7]
print(resultado)
print(len(resultado))

# Modificar un elemento de una lista
lista[1] = 900
print(lista)
# Agregar un elemento a la lista
lista.append(250)
print(lista)
lista.insert(3, 100)
print(lista)
# Reemplazar una sección de la lista
lista[2:4] = [1000, 2000]
print(lista)
lista[2:4] = []
print(lista)
# Lista de listas = matrices
matriz = [ [1,2,3] , [4,5,6], [7,8,9] ]
print(matriz)

paises = ['UK', 'Japan', 'France', 'Italy', 'USA', 'Korea']
print(paises)
print(paises.index('Italy'))
# Invertir lista
paises.reverse()
print(paises)
# Ordenar lista
paises.sort()
print(paises)
# Eliminar el ultimo elemento
paises.pop()
print(paises)
# Eliminar un elemento
paises.remove('France')
print(paises)
