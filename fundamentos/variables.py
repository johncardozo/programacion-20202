a = 4
print(a)
print(type(a))

a = 5.67
print(a)
print(type(a))

a = "universidad santo tomas"
print(a)
print(type(a))

a = False
print(a)
print(type(a))

# Asignacion de una constante
a = 4
# Asignacion de una variable
b = a
# Asignacion de una expresion
c = a + b
print(a)
print(b)
print(c)



