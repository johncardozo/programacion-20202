# Pide datos al usuaio por consola
nombre = input('Digite su nombre: ')
print(nombre)
# Pide datos al usuario
a = input('Digite el valor A: ')
b = input('Digite el valor B: ')
# Convierte los datos a entero
valor_a = int(a)
valor_b = int(b)
# Suma los datos enteros
c = valor_a + valor_b
# Imprime los tipos de datos
print(type(valor_a))
print(type(valor_b))
# Imprime los valores de las variables
print(a, b, c)
# Transforma a entero
x = '123'
x_int = int(x)
# Transforma a str
y = 23
y_str = str(y) 
# Transforma a float
z = '4.5'
z_float = float(z)
# Transforma a booleano
w = 1
w_boolean = bool(w)
print(x_int, y_str, z_float, w_boolean)