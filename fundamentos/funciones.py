
# Definicion de una función
def mostrar_mensaje():
    print('Hola desde una funcion en Python')


def suma(num1, num2):
    resultado = num1 + num2 + 0.5
    return resultado

def es_par(numero):
    if numero % 2 == 0:
        return True
    else:
        return False

# Ejecucion de la funcion
#mostrar_mensaje()

r = suma(3, 4)
print(r)

print(suma(10, 5))
x = suma(10, 5) + suma(30, 40)
print(x)

print(es_par(4))
print(es_par(7))


