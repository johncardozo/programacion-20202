a = 5
b = 3
# Suma
c = a + b
# Resta
d = a - b
# Multiplicacion
e = a * b
# Division (siempre genera un valor float)
f = a / b
# Modulo (residuo de la division)
g = 20 % 6
# Potencia (2 * 2 * 2)
h = 2 ** 3
# Division obteniendo la parte entera
i = a // b
# Muestra las variables por pantalla
print(a, b, c, d, e, f, g, h, i)
