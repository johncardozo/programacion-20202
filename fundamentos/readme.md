# Proyecto de Programación

Este proyecto fue hecho en clase con los estudiantes del curso de Programación Aplicada de USTA.

## Herramientas

- VSCode
- Git
- BitBucket
- Python 3.9
- AWS
- Postgres
- Amazon RDS

Programa desarrollado por John Cardozo