# Inicializacion
contador = 10
# Expresion booleana
while contador >= 0:
    # Instrucciones a ejecutar
    if contador % 2 == 0:
        print(f'{contador} es par')
    else:
        print(f'{contador} es impar')
    # Instrucción de rompimiento
    contador = contador - 1

print('=' * 20)

# Rango: inicio
for elemento in range(5):
    print(elemento)

print("-" * 20)
# Rango: inicio, fin
for elemento in range(5, 10):
    print(elemento)

print("_" * 20)
# Rango: incio, fin, incremento
for elemento in range(5, 22, 3):
    print(elemento)

ciudades = ['bogota', 'medellin', 'cali', 'bucaramanga']
# Recorrido de lista con for
for ciudad in ciudades:
    print(ciudad)

print("*" * 20)   
 
# Recorrido de lista con while
i = 0
while i < len(ciudades):
    print(ciudades[i])
    i = i + 1

