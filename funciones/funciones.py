

def sumar(num1, num2):
    """
    Esta función retorna la suma de num1 y num2.
    """
    return num1 + num2

def restar(num1, num2):
    '''
    Esta función retorna la resta num2 de  num1.
    '''
    return num1 - num2