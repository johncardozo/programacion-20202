# Importar la libreria de acceso a Postgres
import psycopg2
from datos_conexion import dc

# 1. Conexion a la base de datos Postgres
conexion = psycopg2.connect(**dc)

# 2. Crear el cursor que ejecuta las sentencias SQL
cursor = conexion.cursor()

# 3. Crear la sentencia SQL
sql = 'delete from empleados where id=%s'

# 4. Define los parametros
id = input('Digite el id del empleado a eliminar: ')
parametros = (id)

try:
    # 5. Ejecuta la sentencia
    cursor.execute(sql, parametros)

    # 6. Elimina los datos en la BD
    conexion.commit()
    print('Se eliminó el empleado')
except:
    print('Hubo un error. Intente mas tarde.')
finally:
    # 7. Cierra el cursor y la conexion
    cursor.close()
    conexion.close()



