import psycopg2
from datos_conexion import dc

# 1. Conexion a la base de datos Postgres
conexion = psycopg2.connect(**dc)

# 2. Crear el cursor que ejecuta las sentencias SQL
cursor = conexion.cursor()

# 3. Establece la sentencia SQL a ejecutar
sql = 'select * from empleados where id=%s'

# 4. Establece los parametros
id = input('Digite el id del empleado: ')
parametros = (id)

# 5. Ejecuta la sentencia SQL
cursor.execute(sql, parametros)

# 6. Obtiene todos los registros
registros = cursor.fetchall()

# 7. Recorrer los registros
for registro in registros:
    id = registro[0]
    nombres = registro[1]
    print(f'{id} = {nombres}')

# 8. Cierra el cursor y la conexion
cursor.close()
conexion.close()
