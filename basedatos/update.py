# Importar la libreria de acceso a Postgres
import psycopg2
from datos_conexion import dc

# 1. Conexion a la base de datos Postgres
conexion = psycopg2.connect(**dc)

# 2. Crear el cursor que ejecuta las sentencias SQL
cursor = conexion.cursor()

# 3. Crear la sentencia SQL
sql = 'update empleados set nombres=%s where id=%s'

# 4. Define los parametros
id = int(input('Digite el id del empleado a modificar: '))
nombres = input('Digite los nuevos nombres del empleado: ')
parametros = (nombres, id)

# 5. Ejecuta la sentencia
cursor.execute(sql, parametros)

# 6. Guarda los datos en la BD
conexion.commit()

# 7. Cierra el cursor y la conexion
cursor.close()
conexion.close()

print('Se guardaron los datos exitosamente!')

