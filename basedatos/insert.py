# Importar la libreria de acceso a Postgres
import psycopg2
from datos_conexion import dc

# 1. Conexion a la base de datos Postgres
conexion = psycopg2.connect(**dc)

# 2. Crear el cursor que ejecuta las sentencias SQL
cursor = conexion.cursor()

# 3. Crear la sentencia SQL
sql = 'insert into empleado(nombres, email) values(%s, %s)'

# 4. Define los parametros
nombres = input('Digite los nombres del empleado: ')
email = input('Digite el email del empleado: ')
parametros = (nombres, email)

try:
    # 5. Ejecuta la sentencia
    cursor.execute(sql, parametros)

    # 6. Guarda los datos en la BD
    conexion.commit()
    print('Se guardaron los datos exitosamente!')
except:
    print('Hubo un error. Intente mas tarde.')
finally:
    # 7. Cierra el cursor y la conexion
    cursor.close()
    conexion.close()



