# Importar la libreria de acceso a Postgres
import psycopg2
from datos_conexion import dc

# 1. Conexion a la base de datos Postgres
conexion = psycopg2.connect(**dc)

# 2. Crear el cursor que ejecuta las sentencias SQL
cursor = conexion.cursor()

# 3. Ejecuta la sentencia SQL
cursor.execute('select * from empleados order by id')

# 4. Obtiene todos los registros
registros = cursor.fetchall()

# 5. Recorrer los registros
for registro in registros:
    id = registro[0]
    nombres = registro[1]
    print(f'{id} = {nombres}')

# 6. Cierra el cursor y la conexion
cursor.close()
conexion.close()